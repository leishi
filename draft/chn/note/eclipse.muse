** 1 安装Eclipse 
*** 1.1 主程序

*** 1.2 CDT c/c++环境插件
   通过eclipse 自动安装

*** 1.3 Subversion 插件
   通过eclipse 自动安装

** 2 QT
   下载安装qt for eclipse 插件

** 3 CppUnit 
*** 3.1 用MingGW编译
   - 下载 CppUnit
   - 启动MSys (MinGW的shell),进入CppUnit下输入,生成include/CppUnit/config-auto.h
<example>
  ./configure
</example>
   - 启动Eclipse ,创建 Managed Make c++ Library工程,静态,chain tools 选择MinGW。
   - 将CppUnit下src/CppUnit中的*.h 和 *.cpp 全都import到工程中.
   - 开启项目属性页，将CppUnit/include包含进入该Path 
   - 如果编译的是动态库(dll),需要在c/c++Build 中的Gcc C++ Compiler 下的Preprocesser中定义变量 CPPUNIT_DLL_BUILD
   - 编译，产生libcppunit.a文件，拷贝至相应文件夹

*** 3.2 编译QT test Runner
  -  

** 4 
