* <lisp>(setq Title "Emacs23 安装配置")</lisp>
 - 整理：: 石磊

<contents depth="4">
  
** 1 简介
   Emacs 23是Emacs的一个分支，，,它全面构建于unicode上，,，。内部编码以UTF-8为主,很好的支持中文.Debian的源中已经包括了Emacs版本,可惜它的x11显示实在时简陋,所以采用gtk来编译它.

** 2 编译安装Emacs
  - 缺什么dev包自己去用apt-get下载
<source lang="shell">
  #apt-get install ssh
  #apt-get install texinfo cvs
  #cvs -z3 -d&#58;ext&#58;anoncvs@savannah.gnu.org&#58;/cvsroot/emacs co -r emacs-unicode-2 emacs
  #./configure --prefix=/usr --with-x-toolkit=gtk --enable-font-backend --with-xft --with-freetype 
  #make bootstrap
  #make info
  #make install
</source>

** 3 安装CEDET
 - CEDET是Emacs的编程环境,它包括speedbar,semantic,eieio等工具


** 4 安装Cscope
  Cscope是一个源代码浏览程序，,它是一个单独的程序，.
 - 安装cscope

<source lang="shell">
  #apt-get install cscope
</source>

 - 拷贝xcscope.el到/etc/share/emacs/23.0.0/site-lisp
 - 在.emacs中间加上(require xcscope)
 - 为了只是在打开c/c++才加载xscope可以加入

<source lang="c">
(add-hook 'c-mode-common-hook
    '(lambda()
     (require 'xcscope)))
</source>

 - 将cscope-indexer脚本拷贝到系统PATH中
<source lang="shell">
  C-c s I(cscope-index-files) //就可以生成Cscope数据库,就可以在程序中使用cscope了.
</source> 

 - 使用方法
   默认是的快捷键都是绑定到 C-c s 的前缀.：
<source lang="shell">
C-c s s         Find symbol.
C-c s d         Find global definition.
C-c s g         Find global definition (alternate binding).
C-c s G         Find global definition without prompting.
C-c s c         Find functions calling a function.
C-c s C         Find called functions (list functions called
                from a function).
C-c s t         Find text string.
C-c s e         Find egrep pattern.
C-c s f         Find a file.
C-c s i         Find files #including a file.

下面是在搜索到的结果之间切换用的快捷键：

C-c s b         Display *cscope* buffer.
C-c s B         Auto display *cscope* buffer toggle.
C-c s n         Next symbol.
C-c s N         Next file.
C-c s p         Previous symbol.
C-c s P         Previous file.
C-c s u         Pop mark.
</source>
