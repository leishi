* <lisp>
(setq Title "我的笔记")
</lisp>
 - 转载请注明[[http://www.stoneszone.net][出处]],谢谢!

<contents depth="3">

** 1. 工程科学

*** 1.1 科学计算 
**** 1.1.1 Blitz++
   Blitz++是一款高效率的C++数值计算程序模板库。它采用了现代的C++技巧。在保持良好封装和灵活性下，Blitz++有令人难以置信的运行效率。可以和数值计算的鼻祖Fortran一绝高低。Blitz++的实现方法和技巧为我们开发灵活/易维护/高效率的数值程序提供了非常重要的参考。
   - [[./Techniques_scientic_cpp.html][C++的科学计算技巧]]
 
**** 1.1.2 MEEP
***** 1.1.2.1简介
  [[http://ab-initio.mit.edu/wiki/index.php/Meep][MEEP(MIT Electromagnetic Equation Propagation) ]]是MIT开发的一个用时域有限差分法求解麦克斯韦方程组的软件,遵守GPL协定,代码开放.

***** 1.1.2.2 安装
<source lang="shell">
  #apt-get install beep
</source>

**** 1.1.3 CFD
***** 1.1.3.1 CGNS
  - [[./cgns.html][安装]]

**** 1.1.3.2 elsa
  - [[./elsa.html][笔记]]

***** 1.1.3.3 应用
  - [[./cfd_engineer.html][心得]]

***** 1.1.3.4 理论
  - [[./turbulentModel.html][湍流模型]]

***** 1.1.3.2 OpenFOAM

*** 1.2 Propulsion
**** 1.2.1 Inlet

**** 1.2.2 Nozzle


*** 1.3 Stealth
**** 1.3.1 XFDTD
   -[[./xfdtd.html][xfdtd]]

** 2. 程序开发
*** 2.1 QT
**** 2.1.1 QT GPL for windows 安装与eclipse整合 
 - 下载QT for windows mingw 4.3.1 和 devcpp 4.9.9.1（可以去www.qtcn.org下载）
 - 安装 devcpp(试过直接安装mingw， 可是编译qt debug库的时候老在qmake处出错)
 - 安装 QT ,在选择mingw安装目录时要选devcpp安装的主目录。
 - 进入开始菜单选择编译 QT debug库


**** 2.1.2 基于Linux的Windows QT交叉编译 
  QT的一次编写就可实现在Linux,Windows和Mac下运行的跨平台特性和优雅的面向对象设计使得大家对它爱不释手，而且还有完整功能的GPL版本。为了能在一个开发环境下编写代码并可以一次编译出多个不同平台的可执行程序，需要针对每个平台配置编译环境
  - [[./linux_windows_crossdev.html][具体步骤]]

*** 2.2 FreeNAS
   一款基于FreeBSD内核的网络存储。曾荣获[[http://www.sourceforge.net][SourceForge的每月最佳开源项目]]。 
   - [[./freenas-development-manual-zh-0.68.html][我翻译整理的FreeNAS开发手册（]]

*** 2.3 Eclipse 
**** 2.3.1 Eclipse配置
  - [[./eclipse.html]]




*** 2.4 Catia二次开发
**** 2.4.1 [[CatiaCAABase.html]]
**** 2.4.2 [[catia_caa.html]] 
**** 2.4.3 [[catia.html][catia]]

*** 2.5 SourceForge的项目管理
  - [[./sourceforge.html][步骤]]

*** 2.6 VTK 
  VTK为开源的科学可视化程序包,在医疗影像中应用广泛,也很适合cfd的后处理软件开发,gui有QT的支持.
  - [[./vtk.html][笔记]]

** 3. Linux
*** 3.1 Debian 心得
   - [[./debian-install.html][安装与配置]] 

*** 3.2 基于U盘的linux系统
   Linux有无限的可能性，你可以为所欲为的定制它，唯一缺少的就是创意和实践了。
   - [[./linux-usbdisk.html][具体步骤]]

*** 3.3 在Thinkpad上保留系统恢复的Linux引导方法
  Thinkpad的一键恢复还是比较方便的，可恢复到购机预装的windows正版系统。它依赖MBR来启动，安装linux时就需要不破坏MBR
   - [[./thinkpad-debian-install.html][具体步骤]]

*** 3.4. 超级强大的怪兽Emacs
**** 3.4.1 Emacs 23 版本安装配置
  - [[./emacs.html][具体步骤]]

**** 3.4.2 Emacs muse
   非常好用的内容发布系统，本站就是用muse管理。
  - [[./muse.html][muse配置笔记]]
  - [[muse-learn.html][muse语法]]
  - [[pyBlosxom.html][pyBlosxom安装方法]]
      

** 4. English
*** 4.1 Oral English
  - [[./oralEnglish.html]]


