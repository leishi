* <lisp>(setq Title "在Linux上搭建面向Windows的交叉编译开发环境(debian发行版)")</lisp>
 - 作者: [[stoneszone.net][石磊]]
 - 版权: 转载请注明出处

<contents depth="4">


** 1 安装C/C++ 开发环境
<source lang="shell">
  #apt-get gcc-4.2 gcc
  #apt-get g++
</source>

** 2 安装MinGW 
<source lang="shell">
  #apt-get install mingw32
</source>
   - 采用i586-mingw32msvc-gcc就可以为windows编译程序。
   - 对于configure脚本，采用 --target=i586-mingw32msvc 或者 --build=i586-mingw32msvc
   - 如果执行需要mingwm10.dll的化，它位于/usr/share/doc/mingw32-runtime中，拷贝到当前目录就好
   - 运行wine xxx.exe

** 3 安装Eclipse
*** 3.1 安装下载Eclipse 3.3
*** 3.2 安装下载CDT
*** 3.3 安装下载PDT all in one（PHP开发环境) 
*** 3.4 安装Subversive（Subversion 工具)

** 4 安装QT

*** 4.1 在windows 上编译QT-DLL
   - 用MinGW gcc编译QT,将所有的QT目录拷贝到linux（若在linux中用交叉编译实现，需要修改Makefile)

*** 4.2 在linux上安装同样版本的QT
   - 要用不同的目录树
<source lang="shell">
  #apt-get install qt4-dev-tools qt4-qtconfig libqt4-dev qt4-doc qt4-designer
</source>
    
*** 4.3 在Linux的Win拷贝QT版本
   - 拷贝$QTWIN/mkspecs/win32-g++ spec 为 win32-x-g++
   - 然后修改qmake.conf文件，将所有参考gcc/g++的替换为交叉编译的版本。并且将cmd.exe替换为linux版本的shell。
   - 如果你不是编译console程序，确保-mwindows在spec中，否则你会在每个程序启动后都跟一个console
   - 我的修改过得[[./qmake.conf][qmake.conf]]
<source lang="shell">
  QMAKE_CC         =/usr/local/mingw/bin/i586-mingw32-gcc
  ////////////////////////////////////////////////////
  QMAKE_CXX         =/usr/local/mingw/bin/i586-mingw32-g++
  //////////////////////////////////////////////////////////////
  QMAKE_INCDIR      =/usr/local/mingw/include
  QMAKE_INCDIR_QT   =/usr/local/Trolltech/Qt-Win-4.0.1/include
  QMAKE_LIBDIR_QT   =/usr/local/Trolltech/Qt-Win-4.0.1/lib
  /////////////////////////////////////////////////////////////
  QMAKE_LINK        =/usr/local/mingw/bin/i586-mingw32-g++
  QMAKE_LFLAGS      = -mthreads -Wl, -enable-stdcall-fixup -Wl,-enable-auto-import -Wl,-enable-runtime-pseudo-reloc -mwindows
  ////////////////////////////////////////////////////////////
  QMAKE_DIR_SEP    =/
  QMAKE_COPY       =cp
  QMAKE_COPY_DIR   =cp -r
  QMAKE_MOVE       =mv
  QMAKE_DEL_FILE   =rm -f
  QMAKE_MKDIR      =mkdir -p
  QMAKE_DEL_DIR    =rm -rf
  ////////////////////////////////////////////////////////////
  QMAKE_RC         =/usr/local/mingw/bin/i586-mingw32-windres
  QMAKE_ZIP        =zip -r -9
  QMAKE_STRIP      =/usr/local/mingw/i586-mingw32-strip
  ////////////////////////////////////////////////////////////
  QMAKE_MOC		= /usr/bin/moc-qt4
  QMAKE_UIC		= /usr/bin/uic-qt4
  QMAKE_IDC		= /usr/bin/idc-qt4

</source>

*** 4.4 交叉编译QT程序
  - 若要编译面向windows的程序: 用新的交叉编译spec调用qmake,如果你的spec包含对MinGW交叉编译器的绝对路径，你都不需要设置PATH
  - 需要从windows的QT拷贝相应的dll到wine下的windows中的system中，否则wine不能启动编译的程序
<source lang="shell">
  #qmake -project
  #qmake -spec /usr/loca/share/qt-4.3.1-win/mkspecs/win32-x-g++
  #make
  #win debug/xxx.exe
</source>

  - 若要编译面向linux的程序: 直接在第二步用qmake替换即可 


  ***!!注意!!***
 - **linux 版本的QT的工具moc,uic,qmake可用编译，其编译是用linux的QT工具**
 - **mingw工具**
  




