* <lisp>(setq Title "Muse配置笔记")</lisp>
 - Author:  石磊
 - 版权:    转载请注明出处

<contents depth="4">

** 1. 如何在html发布模板中加入页面的标题
  - 在每个页面的头部定义一个lisp变量，并显示为一级标题
<example>
* <li*p>(setq Title "标题")</li*p>
</example>
  - 在html header模板中插入lisp命令
<example>
<title><l*sp>Title</li*p></title>
</example>

** 2. 如何显示最新的更新日期
  - 在你想显示的地方插入如下lisp代码
<example>
<li*p>(current-time-string (nth 5(file-attributes muse-publishing-current-file)))</li*p>
</example>

** 3. 代码高亮
  - muse最新的版本已经包括src tag的代码高亮，,采用htmilize来实现的。.可惜我的总是没有显示，,郁闷至极，,发现[[http://blog.chinaunix.net/u/8057/showart_336268.html][hellwolf]]写的调用source-highlight来实现的代码高亮的方法很有特色，,source-highlight是一个小的shell程序，,可以处理很多的程序语言，转化成html,xhtml,doc,latex等等的输出。.
  - 先安装source-highlight，,然后将下面的patch加入你的.emacs中 
  - hellwolf兄的patch最终结果是代码中嵌入style来实现不是很优雅,我想通过外部css来配置显示，,将它的代码修改了一下,[[./source-highlight.el][代码]].
  - 现在发布的都是xhtml代码,样式在外部给定,这边有个很多css的例子[[http://www.gnu.org/software/src-highlite/style_examples.html][source-highlight站点给出的代码显示的css例子]]。可以参考。.
    



<example>
支持的代码：:
C = cpp.lang
H = cpp.lang
bison = bison.lang
c = cpp.lang
caml = caml.lang
cc = cpp.lang
changelog = changelog.lang
cpp = cpp.lang
cs = csharp.lang
csharp = csharp.lang
diff = diff.lang
eps = postscript.lang
flex = flex.lang
fortran = fortran.lang
h = cpp.lang
hh = cpp.lang
hpp = cpp.lang
htm = html.lang
html = html.lang
java = java.lang
javascript = javascript.lang
js = javascript.lang
l = flex.lang
lang = langdef.lang
langdef = langdef.lang
latex = latex.lang
lex = flex.lang
lgt = logtalk.lang
ll = flex.lang
log = syslog.lang
logtalk = logtalk.lang
lua = lua.lang
ml = caml.lang
mli = caml.lang
outlang = outlang.lang
pas = pascal.lang
pascal = pascal.lang
patch = diff.lang
perl = perl.lang
php = php3.lang
php3 = php3.lang
pl = prolog.lang
pm = perl.lang
postscript = postscript.lang
prolog = prolog.lang
ps = postscript.lang
py = python.lang
python = python.lang
rb = ruby.lang
ruby = ruby.lang
sh = sh.lang
shell = sh.lang
sig = sml.lang
sml = sml.lang
style = style.lang
syslog = syslog.lang
tex = latex.lang
xhtml = xml.lang
xml = xml.lang
y = bison.lang
yacc = bison.lang
yy = bison.lang

支持的输出格式：:
docbook = docbook.outlang
esc = esc.outlang
esc-doc = esc.outlang
html = html.outlang
html-css = css_common.outlang
html-css-doc = cssdoc.outlang
html-doc = htmldoc.outlang
javadoc = javadoc.outlang
latex = latex.outlang
latex-doc = latexdoc.outlang
latexcolor = latexcolor.outlang
latexcolor-doc = latexcolordoc.outlang
texinfo = texinfo.outlang
xhtml = xhtml.outlang
xhtml-css = xhtmlcss.outlang
xhtml-css-doc = xhtmldoc.outlang
xhtml-doc = xhtmldoc.outlang
</example>

** 4 一次性全部发布工程
  - C-u C-c C-p
