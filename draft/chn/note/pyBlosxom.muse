* <lisp>(setq Title "安装pyblosxom")</lisp>

<contents depth="4">

  pyblosxom是一个面向Haker的 blog程序，,emacs muse可以支持直接发布到pyblosxom.在dreamhost上安装pyblosxom的步骤如下.

** 初始安装
  - 下载pyblosxom源码
  - 在dreamhost上创建需要的目录结构
<source lang="shell">
  -stoneszone.net/journal/
                        ./entries
                        ./plugins
                        ./flavours 
</source>
  - 将解压缩后的Pyblosxom目录上传至journal下,其中为pyblosxom的python库文件
  - 拷贝解压缩后的web目录下的 config.py和pyblosxom.cgi文件到journal下
  - 修改 config.py,

<source lang="shell">
# Blog的主要语言用于Rss Feed
py['blog_language'] = "zh"

# 输出编码
py['blog_encoding'] = "utf-8"

# Blog日志存放路径
py['datadir'] = "entries"

#插件文件路径
py['plugindir']="plugins"

# 日志文件存放路径
py['logdir'] = "logs"
</source> 
  
    - 测试,rsh登录到站点,运行./pyblosxom.cgi,查看输出是否正常,如果不能执行,改属性为755
    - 在entries目录下输入测试blog条目,第一行为标题
<source lang="shell">
       This is my first post
       <p>
           This is my first post with PyBlosxom.
       </p>
</source>

    - 在浏览器中输入http://www.stoneszone.net/journal/pyblosxom.cgi
    - Apache 重定向,实现http://www.stoneszone.net/journal => http://www.stoneszone.net/journal/pyblosxom.cgi
<source lang="C">
rewriteengine on 
rewriterule ^(.*)$ pyblosxom.cgi
</source>

** 安装插件
   每次修改了config.py,在命令行里运行一下Pyblosxom.cgi,看一下输出有没有问题.
*** xmlrpc.py
  在pyBlosxom网站下载contrib包,其中包括xmlrpc.将其目录下的xmlrpc.py拷贝至plugins目录下,很多的插件都要用它

*** comments
  将contrib包中的comments.py拷贝至plugins中,并修改config.py的plugin加载选项,加入comments

*** comments spam过滤器
  下载magicword,拷贝至plugins目录,在config.py中加载它,并且加入
<source lang="c">
  py['mw_question'] = "What is the first word in this sentence?"
  py['mw_answer'] = "what"
</source>

