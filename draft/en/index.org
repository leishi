#+SETUPFILE: ../templates/level-0-en.org
#+INCLUDE: ../templates/level-0-en.org
#+TITLE:Lei Shi's Home Page
#+BEGIN_HTML
<div>

<div class="lavaLamp">
<li class="current"><a title="Site Home" href="./index.html">Home</a> </li>
<li><a title="Resume" href="./resume.html">Resume</a></li>
<li><a title="Blog" href="./research.html">Research</a></li>
<li><a title="Projects" href="./teaching.html">Teaching</a></li>
<li><a title="Wiki" href="./wiki.html">Wiki</a></li>
<li><a title="Download" href="./download.html">Download</a></li>
<li><a title="Notes" href="./note/index.html">Notes</a></li>
<li><a title="Hobbies" href="./hobbies.html">Hobbies</a></li>
<li><a title="Links" href="./links.html">Links</a></li> 
<li><a title="Links" href="../chn/">Chinese</a></li> 
</div>


<div id="intro">
<div class="self-pic">
<img src="../images/stone.jpg">
</div>

<div class="self-info">
<h2>Lei Shi </h2>
<li>Ph.D. Student </li> 
<li>Department of Aerospace Engineering </li>
<li>Iowa State University </li>
<li>Room 0233 Howe Hall, Ames, IA 50011 </li>
<li>Phone: (515) xxx-xxxx </li>
</div>
</div>
<div class="clear"></div>

#+END_HTML



* Recent News and Quick Links:
 - Rewrite website using [[http://orgmode.org/][org mode of Emacs]].

* Welcome to Visit My Website:
#+BEGIN_HTML
<div style="padding:0.5em 0.5em 3em 2em">
<script type="text/javascript" src="http://jf.revolvermaps.com/r.js"></script>
<script type="text/javascript">rm_f1st('0','180','true','false','ffffff','5tj4602eib9','true','ff0000');</script><noscript><applet codebase="http://rf.revolvermaps.com/j" code="core.RE" width="180" height="180" archive="g.jar"><param name="cabbase" value="g.cab" /><param name="r" value="true" /><param name="n" value="false" /><param name="i" value="5tj4602eib9" /><param name="m" value="0" /><param name="s" value="180" /><param name="c" value="ff0000" /><param name="v" value="true" /><param name="b" value="ffffff" /><param name="rfc" value="true" /></applet></noscript>
</div>
#+END_HTML
