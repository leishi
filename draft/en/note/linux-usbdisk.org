* <lisp>(setq Title "自己制作usb上的linux系统")</lisp>  
 - 作者: [[stoneszone.net][石磊]]
 - 版权: 转载请注明出处

<contents depth="4">


** 1  创建工作环境

<source lang="shell">
  $mkdir /usr/local/embase/
  $cd /usr/local/embase
  $mkdir rootfs               #根文件
  $mkdir boot                 #引导所需文件
</source>

** 2 编译busybox
  下载并解压缩进入配置解界面，
  - 编译busybox为静态连接，编译静态会报错，要修改发出警报的makefile和其中一个文件，关掉警报。
  - 选择Installation Options下 Don't use /usr，否则会安装到宿主机上

<source lang="shell">
  $cd busybox
  $make menuconfig
  $make
  $make install
</source>
  
  完成后会在busybox目录下生成_install目录。

** 3 创建根文件系统
*** 3.1 创建目录结构

<source lang="shell">
  $cd /usr/local/embase/rootfs
  $mkdir etc usr var tmp proc home root lib
</source>


*** 3.2  安装busybox

<source lang="shell">
  $cp -R busybox/_install/* /usr/local/embase/rootfs
</source>

*** 3.3 建立设备文件
  直接拷贝宿主机的/dev目录

<source lang="shell">
  $cp -R/dev /usr/local/embase/rootfs
</source>

  也可以用mknod手工建立,如创建/dev/console，字符设备c, 主设备号5，次1

<source lang="shell">
  $mknod console c 5 1
</source>

*** 3.4 配置/etc

**** 3.4.1  /etc/init.d 目录
   直接拷贝busybox的启动目录。
<source lang="shell">
  $cp -R busybox-1.00/examples/bootflopyp/etc/init.d /usr/local/embase/rootfs/etc/
</source>

**** 3.4.2 /etc/busybox.conf 为空文件

**** 3.4.3 /etc/fstab

<source lang="shell">
  /dev/fd0   /            ext2          defaults 0 0
  none       /proc        proc          defaults 0 0
  /dev/cdrom /mnt/cdrom   udf,iso9660   noauto,owner,kudzu,ro 0 0
  /dev/fd0    /mnt/floppy auto          noauto,owner,kudzu 0 0
</source>

**** 3.4.4 /etc/group

<source lang="shell">
   root:x:0:root
</source>

**** 3.4.5 /etc/inittab
  拷贝ｂｕｓｙｂｏｘ的示范ｉｎｉｔｔａｂ指ｅｔｃ

**** 3.4.6 /etc/issue

<source lang="shell">
  Embase NAS 0.1
</source>

**** 3.4.7 /etc/motd mtab 

**** 3.4.8 /etc/passwd

<source lang="shell">
  root::0:0:root:/root:/bin/ash
</source>

**** 3.4.9 /etc/profile

<source lang="shell">
  # /etc/profile: system-wide .profile file for the Bourne shells
  echo
  echo
  export PS1="[\u@\h \w]\$"
  echo "Done"
  alias ll='ls -l'
  alias du='du -h'
  alias df='df -h' 
  alias rm='rm -i'
  echo
</source>

**** 3.4.10 /etc/resolv.conf(可选)
  
<source lang="shell">
  nameserver 202.96.209.5
  nameserver 202.96.209.6
</source>

**** 3.4.11 /etc/shadow 和 /etc/shadow-
  口令文件，其中的计算后口令字串从本机拷贝。
shadow

<source lang="shell">
  root:$1$$adltAB9Sr/MSKqylIvSJT/:12705:0:99999:7:::
</source> 

shadow-

<source lang="shell">
  root:$1$DWU.tenP$B7ANiXoGoiZMwJR6Ih8810:12705:0:99999:7:::
</source>

**** 3.4.12 /etc/init.d/rcS 启动后第一个执行脚本(inittab中配置)
  确保有可执行权限，且内容为：

<source lang="shell">
  #! /bin/sh
  mount -o remount,rw /


  /bin/mount -a
  >/etc/mtab
echo
echo
echo
echo
echo -en "\t\tWelcom to \\033[0;32mEmbase NAS\\033[0;39m\n"
echo -en "\\033[0;36m\n"
echo
echo -en "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\t\t\n"
echo -en "+ Embase NAS V 0.1 \t\t\n"
echo -en "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\t\t\n"
echo -en "\\033[0;39m\n"
hostname BabyLinux
</source>


** 4 创建boot目录

*** 4.1  拷宿主机/boot/下grub目录的stage1 stage2 e2fs_stage1_5 menu.list 至/ｕｓｒ/ｌｏｃａｌ/ｅｍｂａｓｅ/ｂｏｏｔ/ｇｒｕｂ/

*** 4.2 创建initrd  
    为了等待usb设备，mount根文件系统，需要重新创建/boot/initrd.img-2.6.15-1-486 文件。
    用于加载内核初始化时相应模块的ramdisk。

   -修改/etc/mkinitrd/mkinitrd.conf DELAY=20 
   -修改/etc/mkinitrd/modules   添加 启动需要加载的用于usb的modules

    <source lang="shell">
     ehci_hcd
     uhci_hcd
     ohci_hcd
     usb-storage
    </source>
   
   <source lang="shell">
    $mkinitrd -o initrd.img-2.6.15-1-486 2.6.15-1-486  #与ls /lib/modules返回的内核版本号一致
    $cp initrd.img-2.6.15-1-486 /usr/local/emabse/boot/
   </source>

*** 4.3 编辑ｍｅｎｕ.ｌｉｓｔ

<source lang="shell">
 title embase
 root (hd0,0)                                 #hd0代表第几个硬盘，从０开始,指示grub的ｓｔａｇｅ１,ｓｔａｇｅ２ 文件位于那个分区
 kernel /boot/vmlinuz root=/dev/sda1   #指示到那里去寻找内核，以及根文件系统的分区
 initrd /boot/initrd                   #启动ramdisk加载内核模块和等待usb文件系统
</source>


** 5 编译安装内核
  内核加载的module位于/lib/modules下。与所编译的内核一一对应。

*** 5.1 拷贝内核至 /boot
    <source lang="shell">
    $cp /boot/vmlinuz-2.6.15-1-486      /usr/local/embase/booot
    </source>

*** 5.2 拷贝modules至/lib/modules
    <source lang="shell">
    $cp -rf /lib/modules /usr/local/embase/rootfs/lib/
    </source>
 
*** 5.3 创建vmlinuz连接
    <source lang="shell">
    $cd /usr/local/embase/boot
    $ln -s vmlinuz-2.6.15-1-486 vmlinuz
    </source>
** 6 创建目标安装盘分区

<source lang="shell">
  $fdisk /dev/sda
</source>


** 7 创建文件系统

<source lang="shell">
  $mkfs.ext2 -m0 /dev/sda0
  $mount /dev/sda0 /ｍｎｔ
</source>

** 8 拷贝所有到目标安装盘

<source lang="shell">
  $cp -rf /usr/local/embase/rootfs/* /mnt
  $cp -rf /usr/local/embase/boot     /mnt
</source>


** 9 安装grub至目标盘
  suse安装有问题，到ｄｅｂｉａｎ安装。

<source lang="shell">
 $grub
 >root (hd1,0)     #寻找grub的stage1,stage2安装分区，hd1代表第二块硬盘，如果宿主机为hda,则sda为第二块硬盘，0为第一个分区。
 >setup (hd1)      #安装grub至第二块硬盘
</source> 

** 10 安装应用软件

*** 10.1 lighttpd

<source lang="shell">
 $cp /usr/sbin/lighttpd /usr/local/embase/rootfs/usr/sbin
 $cp -rf /etc/lighttpd /usr/local/embase/rootfs/etc/
 $cp -rf /usr/lib/lighttpd/ /usr/local/embase/rootfs/usr/lib
 $cp -rf /usr/share/lighttpd/ /usr/local/embase/rootfs/usr/share/
</source>
